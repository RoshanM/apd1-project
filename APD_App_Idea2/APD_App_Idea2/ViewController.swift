//
//  ViewController.swift
//  APD_App_Idea2
//
//  Created by Roshan Mykoo on 9/10/15.
//  Copyright (c) 2015 Roshan Mykoo. All rights reserved.
//

import UIKit
import CoreMotion
import EventKit
import EventKitUI
import HealthKit


// operator overloading the == operator to check two structs of this type.
func ==(lhs: EKSourceType, rhs: EKSourceType) -> Bool {
    return lhs.value == rhs.value
}


// MARK: - EKCalendarChooserDelegate
extension ViewController: EKCalendarChooserDelegate {
    func calendarChooserDidCancel(calendarChooser: EKCalendarChooser!) {
        println("Canceled choosing a calendar");
        dismissViewControllerAnimated(true, completion: nil);
    }
    
    func calendarChooserDidFinish(calendarChooser: EKCalendarChooser!) {
        println("Chose a calendar to use");
        dismissViewControllerAnimated(true, completion: nil);
    }
}

// MARK: - EKEventEditViewDelegate
extension ViewController: EKEventEditViewDelegate {
    func eventEditViewController(controller: EKEventEditViewController!, didCompleteWithAction action: EKEventEditViewAction) {
        println("Done!");
    }
}






class ViewController: UIViewController, EKCalendarChooserDelegate, EKEventEditViewDelegate  {
    
    @IBOutlet weak var userAct: UILabel!
    @IBOutlet weak var userSteps: UILabel!
    @IBOutlet weak var userFloors: UILabel!
    @IBOutlet weak var userDistance: UILabel!
    
    var stepStorage = String();
    var floorStorage = String();
    var distanceStorage = String();
    
    
    // The eventStore which will be used as the point of contact for accessing calendar and reminder data
    let eventStore = EKEventStore();
    var currentDate = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: NSDateFormatterStyle.ShortStyle, timeStyle: NSDateFormatterStyle.ShortStyle)
    

    //Creating Activity Manager
    let activityManager = CMMotionActivityManager()
    let pedoMeter = CMPedometer()
    

    override func viewDidLoad() {
         super.viewDidLoad()
        
        // check to see if we already have authorization
        let status = EKEventStore.authorizationStatusForEntityType(EKEntityTypeEvent);
        
        switch status {
        case .NotDetermined:
            eventStore.requestAccessToEntityType(EKEntityTypeEvent, completion: handler);
        case .Restricted:
            println("Restricted");
        case .Denied:
            println("Denied");
        case .Authorized:
            println("Authorized!");
        }
        
        
        
        
        
        let cal = NSCalendar.currentCalendar()
        var comps = cal.components(NSCalendarUnit.CalendarUnitYear | NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitHour | NSCalendarUnit.CalendarUnitMinute | NSCalendarUnit.CalendarUnitSecond, fromDate: NSDate())
        
        comps.hour = 0;
        comps.minute = 0;
        comps.second = 0;
        
        let timeZone = NSTimeZone.systemTimeZone()
        cal.timeZone = timeZone
        
        let midnightOfToday = cal.dateFromComponents(comps)!
        
        if(CMMotionActivityManager.isActivityAvailable()){
            self.activityManager.startActivityUpdatesToQueue(NSOperationQueue.mainQueue(), withHandler: { (data: CMMotionActivity!) -> Void in
                if(data.stationary == true){
                    self.userAct.text = "Stationanry"
                } else if (data.walking == true){
                    self.userAct.text = "Walking"
                } else if (data.running == true){
                    self.userAct.text = "Running"
                } else if (data.automotive == true){
                    self.userAct.text = "Automotive"
                }
            })
        }
        
        
        if(CMPedometer.isStepCountingAvailable()){
            let fromDate = NSDate(timeIntervalSinceNow: -86400 * 7)
            self.pedoMeter.queryPedometerDataFromDate(fromDate, toDate: NSDate()) {(data: CMPedometerData!, error) -> Void in
                dispatch_async(dispatch_get_main_queue(), {() -> Void in
                    if(error == nil){
                        self.userSteps.text = "\(data.numberOfSteps)"
                        self.stepStorage = "\(data.numberOfSteps)"
                    }
                })
                }
            
            self.pedoMeter.startPedometerUpdatesFromDate(midnightOfToday) {(data: CMPedometerData!, error) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    if(error == nil){
                        self.userSteps.text = "\(data.numberOfSteps)"
                        self.stepStorage = "\(data.numberOfSteps)"
                    }
                })
            }
        }
        
        if(CMPedometer.isFloorCountingAvailable()){
            let fromDate = NSDate(timeIntervalSinceNow: -86400 * 7)
            self.pedoMeter.queryPedometerDataFromDate(fromDate, toDate: NSDate()) {(data: CMPedometerData!, error) -> Void in
                dispatch_async(dispatch_get_main_queue(), {() -> Void in
                    if(error == nil){
                        self.userFloors.text = "\(data.floorsAscended)"
                        self.floorStorage = "\(data.floorsAscended)"
                    }
                })
            
            }
            self.pedoMeter.startPedometerUpdatesFromDate(midnightOfToday) { (data: CMPedometerData!, error) -> Void in
                dispatch_async(dispatch_get_main_queue(), {() -> Void in
                    if(error == nil){
                        self.userFloors.text = "\(data.floorsAscended)"
                        self.floorStorage = "\(data.floorsAscended)"
                    }
                })
            }
        }
        
        if(CMPedometer.isDistanceAvailable()){
            let fromDate = NSDate(timeIntervalSinceNow: -86400 * 7)
            self.pedoMeter.queryPedometerDataFromDate(fromDate, toDate: NSDate()) {(data: CMPedometerData!, error) -> Void in
                if(error == nil){
                    self.userDistance.text = "\(Double(data.distance))"
                    self.distanceStorage = "\(Double(data.distance))"
                }
            }
            self.pedoMeter.startPedometerUpdatesFromDate(midnightOfToday) {(data: CMPedometerData!, error) -> Void in
                dispatch_async(dispatch_get_main_queue(), {() -> Void in
                    if(error == nil){
                        self.userDistance.text = "\(data.distance)"
                        self.distanceStorage = "\(data.distance)"
                    }
                })

            }
        }
        

        
        // Do any additional setup after loading the view, typically from a nib.
    }

    private func handler(granted: Bool, error: NSError!) {
        if let error = error {
            println("RequestAccessToEntityType Failed With Error: \(error)");
            return;
        }
        
        if granted {
            println("You've been granted access successfully!");
            
            // Create a calendar, give it properties (most importantly the source), add it to the event store
            // Now that we have access to the eventStore for events, let's create our very own calendar that can hold events and store that on the device.
            let calendar = EKCalendar(forEntityType: EKEntityTypeEvent, eventStore: eventStore);
            // set the calendar's properties
            calendar.title = "My FitCal";
            calendar.CGColor = UIColor.purpleColor().CGColor;
            // find the local source in our event store, so we can set our calendar's source to it.
            
            // NOTE: Weird Xcode Bug
            // If you restart the simulator, you must let it 'settle' before running it. This won't happen on an actual device.
            
            for source in eventStore.sources() as! [EKSource] {
                // we created an overloaded == operator in global scope :]
                if source.sourceType == EKSourceTypeLocal {
                    // we found our local source, so we're done searching
                    calendar.source = source;
                    break;
                }
            }
            
            var saveError: NSError? = nil;
            eventStore.saveCalendar(calendar, commit: true, error: &saveError);
            
            if let saveError = saveError {
                println("Saving Calendar failed with error: \(saveError)");
            } else {
                println("Successfully saved \(calendar.title) to Calendars app");
            }
            
            
            
            // MARK: - CREATING AN EVENT
            /*********** CREATING AN EVENT ************/

            let event = EKEvent(eventStore: eventStore);
            // let's define the event's title and time frame
            event.title = "Your Fitness data for \(currentDate)";
            event.startDate = NSDate();
            event.endDate = event.startDate.dateByAddingTimeInterval(1200);
            // tell our event which calendar it should save to
            event.calendar = calendar;
            event.notes = "Your distance traveld on foot today :\(distanceStorage), \n Floors you Climbed today: \(floorStorage), \n Your steps made toady: \(stepStorage)"
            
            // save the event to our calendar
            saveError = nil;
            eventStore.saveEvent(event, span: EKSpanThisEvent, commit: true, error: &saveError);
            
            if let saveError = saveError {
                println("Saving event to calendar failed with error: \(saveError)");
            } else {
                println("Successfully saved '\(event.title)' to \(event.calendar.title)' calendar");
                
                println(event.notes)
            }
        } else {
            println("You were not granted access to Calendars. Handle this appropriately");
        }
    }
    
    
    
    @IBAction func saveFitEvent(sender: UIButton) {
        if EKEventStore.authorizationStatusForEntityType(EKEntityTypeEvent) != .Authorized {
            return;
        }
        
        // Create the EventEditViewController
        let eventEVC = EKEventEditViewController();
        // tell it to use our eventStore
        eventEVC.eventStore = eventStore;
        // set its delegate to ourself so we can respond to certain events
        eventEVC.editViewDelegate = self;
        
        // present our eventEVC
        presentViewController(eventEVC, animated: true, completion: nil);
        

    }
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

